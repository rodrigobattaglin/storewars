package com.example.rodrigo.storewars.Auxiliar;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rodrigo.storewars.Objects.Product;
import com.example.rodrigo.storewars.R;

import java.util.List;

/**
 * Created by Rodrigo on 12/12/2017.
 */

public class AdapterProducts extends BaseAdapter {

    private final List<Product> products;
    private final Activity act;

    public AdapterProducts(List<Product> products, Activity act) {
        this.products = products;
        this.act = act;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.layout_item, parent, false);
        Product product = products.get(position);

        TextView title = (TextView) view.findViewById(R.id.txtTitle);
        TextView price = (TextView) view.findViewById(R.id.txtPrice);
        TextView seller = (TextView) view.findViewById(R.id.txtSeller);
        ImageView thumbnail = (ImageView) view.findViewById(R.id.imgThumbnail);

        title.setText(product.getTitle());
        price.setText("R$" + String.format("%.2f", product.getPrice()));
        seller.setText(product.getSeller());
        thumbnail.setImageBitmap(product.getThumbnail());

        return view;
    }
}
