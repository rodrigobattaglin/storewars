package com.example.rodrigo.storewars.Database;

import android.provider.BaseColumns;

/**
 * Created by Rodrigo on 12/12/2017.
 */

public class Database {
    public Database() {

    }

    public static class ProductsDB implements BaseColumns {

        public static final String TABLE_NAME = "tb_product";
        public static final String COLUMN_ID = "id_product";
        public static final String COLUMN_TITLE = "ts_title";
        public static final String COLUMN_PRICE = "db_price";
        public static final String COLUMN_ZIPCODE = "ts_zipcode";
        public static final String COLUMN_SELLER = "ts_seller";
        public static final String COLUMN_URL = "ts_url";
        public static final String COLUMN_PHOTO = "bi_photo";
        public static final String COLUMN_DATE = "dt_date";

        public static final String CREATE_TABLE =
                "CREATE TABLE IF NOT EXISTS " +
                        TABLE_NAME + " (" +
                        COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        COLUMN_TITLE + " VARCHAR(250), " +
                        COLUMN_PRICE + " DOUBLE, " +
                        COLUMN_ZIPCODE + " VARCHAR(9), " +
                        COLUMN_SELLER + " VARCHAR(250), " +
                        COLUMN_URL + " VARCHAR(250), " +
                        COLUMN_PHOTO + " BLOB, " +
                        COLUMN_DATE + " DATE)";
    }

    public static class TransactionDB implements BaseColumns {

        public static final String TABLE_NAME = "tb_transaction";
        public static final String COLUMN_ID = "id_transaction";
        public static final String COLUMN_FOUR_NUMBER = "ts_four_number";
        public static final String COLUMN_NAME = "ts_name";
        public static final String COLUMN_DATETIME = "dt_datetime";
        public static final String COLUMN_VALUE = "db_value";

        public static final String CREATE_TABLE =
                "CREATE TABLE IF NOT EXISTS " +
                        TABLE_NAME + " (" +
                        COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        COLUMN_FOUR_NUMBER + " CHAR(4), " +
                        COLUMN_NAME + " VARCHAR(250), " +
                        COLUMN_DATETIME + " DATETIME, " +
                        COLUMN_VALUE + " DOUBLE)";
    }
}
