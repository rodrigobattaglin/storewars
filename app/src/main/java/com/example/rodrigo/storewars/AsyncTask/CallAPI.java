package com.example.rodrigo.storewars.AsyncTask;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.rodrigo.storewars.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Rodrigo on 16/12/2017.
 */

public class CallAPI extends AsyncTask<String, String, String>{
    private Exception mLastError = null;
    private MainActivity mainActivity = null;
    ProgressDialog mProgress;

    private String urlAPI = "https://private-2f8bb-storewars1.apiary-mock.com/transactions";
    private long transactionid;

    public CallAPI(MainActivity mainActivity, long transactionid){
        this.mainActivity = mainActivity;
        this.transactionid = transactionid;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgress = new ProgressDialog(mainActivity);
        mProgress.setTitle("Enviando dados...");
        mProgress.setCancelable(true);
        mProgress.setCanceledOnTouchOutside(false);
        mProgress.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                cancel(false);
            }
        });
        mProgress.show();
    }

    @Override
    protected String doInBackground(String... params) {
        String response = null;
        try {
            response = postData(params);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return response;
    }

    private String postData(String... params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        try{
            URL url = new URL(urlAPI);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.connect();

            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("card_number", params[0]);
            jsonObject.accumulate("value", params[1].replace("R$", "").replace(",", ""));
            jsonObject.accumulate("cvv", params[2]);
            jsonObject.accumulate("card_holder_name", params[3]);
            jsonObject.accumulate("exp_date", params[4]);
            Log.d("REQUEST", jsonObject.toString());

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(jsonObject.toString());
            wr.flush();
            wr.close();

            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            Log.d("RESPONSE", result.toString());
            rd.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    @Override
    protected void onPostExecute(String output) {
        mProgress.dismiss();
        if (output == null || output.isEmpty()) {
            Toast.makeText(mainActivity, "Retorno vazio", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mainActivity, "Dados enviados com sucesso", Toast.LENGTH_SHORT).show();
            mainActivity.price = 0;
            mainActivity.txtActualBuy.setText("Total da Compra Atual: R$" + String.format("%.2f", mainActivity.price));
        }
    }

    @Override
    protected void onCancelled() {
        mProgress.hide();
        if (mLastError != null) {
            Toast.makeText(mainActivity, mLastError.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mainActivity, "Envio cancelado", Toast.LENGTH_SHORT).show();
        }
    }
}
