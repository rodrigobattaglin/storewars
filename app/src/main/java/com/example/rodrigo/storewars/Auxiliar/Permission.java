package com.example.rodrigo.storewars.Auxiliar;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

/**
 * Created by Rodrigo on 12/12/2017.
 */

public class Permission {
    private static String[] PERMISSIONS;

    public Permission(String[] MY_PERMISSIONS) {
        super();
        PERMISSIONS = MY_PERMISSIONS;
    }

    public Permission() {
        super();
    }


    public String[] getPERMISSIONS() {
        return PERMISSIONS;
    }

    public void setPERMISSIONS(String[] pERMISSIONS) {
        PERMISSIONS = pERMISSIONS;
    }


    public static boolean hasPermissions(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && PERMISSIONS != null) {
            for (String permission : PERMISSIONS) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkGranted(int[] grantResults){
        if(grantResults.length > 0){
            boolean resultado = true;
            for (int result:grantResults){
                resultado = resultado && (result == PackageManager.PERMISSION_GRANTED);
            }
            return resultado;
        }
        else {
            return false;
        }
    }
}
