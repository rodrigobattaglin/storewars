package com.example.rodrigo.storewars.Auxiliar;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.rodrigo.storewars.Objects.Transaction;
import com.example.rodrigo.storewars.R;

import java.util.List;

/**
 * Created by Rodrigo on 16/12/2017.
 */

public class AdapterTransactions extends BaseAdapter{
    private final List<Transaction> transactions;
    private final Activity act;

    public AdapterTransactions(List<Transaction> transactions, Activity act) {
        this.transactions = transactions;
        this.act = act;
    }

    @Override
    public int getCount() {
        return transactions.size();
    }

    @Override
    public Object getItem(int position) {
        return transactions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.layout_transaction, parent, false);
        Transaction transaction = transactions.get(position);

        TextView id = (TextView) view.findViewById(R.id.txtId);
        TextView name = (TextView) view.findViewById(R.id.txtName);
        TextView numbers = (TextView) view.findViewById(R.id.txtNumbers);
        TextView datetime = (TextView) view.findViewById(R.id.txtDatetime);
        TextView value = (TextView) view.findViewById(R.id.txtValue);

        id.setText(transaction.getId() + "");
        name.setText(transaction.getName());
        numbers.setText(transaction.getLast4numbers() + "");
        datetime.setText(transaction.getDatetime());
        value.setText("R$" + String.format("%.2f", transaction.getValue()));

        return view;
    }
}
