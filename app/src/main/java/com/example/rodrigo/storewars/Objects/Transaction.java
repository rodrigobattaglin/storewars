package com.example.rodrigo.storewars.Objects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.rodrigo.storewars.Database.Database;
import com.example.rodrigo.storewars.Database.SQLite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rodrigo on 16/12/2017.
 */

public class Transaction {
    private long id;
    private double value;
    private String datetime;
    private int last4numbers;
    private String name;

    public Transaction(){

    }

    public Transaction(long id) {
        this.id = id;
    }

    public Transaction(double value, String datetime, int last4numbers, String name) {
        this.value = value;
        this.datetime = datetime;
        this.last4numbers = last4numbers;
        this.name = name;
    }

    public Transaction(long id, double value, String datetime, int last4numbers, String name) {
        this.id = id;
        this.value = value;
        this.datetime = datetime;
        this.last4numbers = last4numbers;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public int getLast4numbers() {
        return last4numbers;
    }

    public void setLast4numbers(int last4numbers) {
        this.last4numbers = last4numbers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean selectTransaction(Context context) {
        SQLite sqLite = new SQLite(context);
        Cursor resp = sqLite.selectQuery(Database.TransactionDB.TABLE_NAME, Database.TransactionDB.COLUMN_ID, id);
        if (resp.getCount() > 0){
            resp.moveToFirst();
            for (int i = 0; i < resp.getColumnCount(); i++){
                if(resp.getColumnName(i).equals(Database.TransactionDB.COLUMN_NAME)) name = resp.getString(i);
                else if(resp.getColumnName(i).equals(Database.TransactionDB.COLUMN_FOUR_NUMBER)) last4numbers = resp.getInt(i);
                else if(resp.getColumnName(i).equals(Database.TransactionDB.COLUMN_DATETIME)) datetime = resp.getString(i);
                else if(resp.getColumnName(i).equals(Database.TransactionDB.COLUMN_VALUE)) value = resp.getDouble(i);
            }
        }
        sqLite.close();
        return resp.getCount() > 0;
    }

    public List<Transaction> selectTransactions(Context context) {
        List<Transaction> listTransaction = new ArrayList<Transaction>();
        SQLite sqLite = new SQLite(context);
        Cursor resp = sqLite.selectALL(Database.TransactionDB.TABLE_NAME, Database.TransactionDB.COLUMN_ID);
        if (resp.getCount() > 0){
            resp.moveToFirst();
            while (!resp.isAfterLast()) {
                Transaction transaction = new Transaction();
                for (int i = 0; i < resp.getColumnCount(); i++){
                    if(resp.getColumnName(i).equals(Database.TransactionDB.COLUMN_ID)) transaction.setId(resp.getLong(i));
                    else if(resp.getColumnName(i).equals(Database.TransactionDB.COLUMN_NAME)) transaction.setName(resp.getString(i));
                    else if(resp.getColumnName(i).equals(Database.TransactionDB.COLUMN_FOUR_NUMBER)) transaction.setLast4numbers(resp.getInt(i));
                    else if(resp.getColumnName(i).equals(Database.TransactionDB.COLUMN_DATETIME)) transaction.setDatetime(resp.getString(i));
                    else if(resp.getColumnName(i).equals(Database.TransactionDB.COLUMN_VALUE)) transaction.setValue(resp.getDouble(i));
                }
                listTransaction.add(transaction);
                resp.moveToNext();
            }
        }
        sqLite.close();
        return listTransaction;
    }

    public long insertTransaction(Context context){
        SQLite sqLite = new SQLite(context);
        ContentValues cv = new ContentValues();

        cv.put(Database.TransactionDB.COLUMN_NAME, name);
        cv.put(Database.TransactionDB.COLUMN_FOUR_NUMBER, last4numbers);
        cv.put(Database.TransactionDB.COLUMN_DATETIME, datetime);
        cv.put(Database.TransactionDB.COLUMN_VALUE, value);
        id = sqLite.insertQuery(Database.TransactionDB.TABLE_NAME, cv);
        Log.d("ID", id + "");
        sqLite.exportLocalDatabase();
        sqLite.close();
        return id;
    }
}
