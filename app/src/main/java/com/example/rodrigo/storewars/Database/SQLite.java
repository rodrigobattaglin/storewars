package com.example.rodrigo.storewars.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.nio.channels.FileChannel;

/**
 * Created by Rodrigo on 12/12/2017.
 */

public class SQLite extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "storewars.db";

    //public SQLiteDatabase sqLiteDatabase;

    public SQLite(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //Log.d("TESTE DE CRIAÇÃO", "FIM CONSTRUTOR");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //this.sqLiteDatabase = sqLiteDatabase;
        //Log.d("TESTE DE CRIAÇÃO", "FIM CREATE");
        createTables(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Database.ContactDB.TABLE_NAME);
        dropTables(sqLiteDatabase);
        onCreate(sqLiteDatabase);
    }

    public void createTables(SQLiteDatabase sqLiteDatabase) {
        //sqLiteDatabase.execSQL(Database.ContactDB.CREATE_TABLE);
        Log.d("TESTE DE CRIAÇÃO", "INÍCIO CREATE");
        for (Class c : Database.class.getDeclaredClasses()) {
            //Log.d("TESTE DE CRIAÇÃO", c.getName());
            Field[] fields = c.getDeclaredFields();
            for(Field f : fields) {
                try {
                    Object o = f.get(c);
                    if(f.getName().equals("CREATE_TABLE")) {
                        Log.d("TESTE DE CRIAÇÃO", f.get(c).toString());
                        sqLiteDatabase.execSQL(f.get(c).toString());
                    }
                } catch (Exception e) {
                    Log.d("SQLite - ERROR", "Erro ao criar tabela da classe " + c.getName() + " . Será que falta alguma tabela dependente?");
                }
            }
        }
    }

    public void dropTables(SQLiteDatabase sqLiteDatabase) {
        //sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Database.ContactDB.TABLE_NAME);
        for (Class c : Database.class.getDeclaredClasses()) {
            //System.out.println(c.getName());
            Field[] fields = c.getDeclaredFields();
            for(Field f : fields) {
                try {
                    Object o = f.get(c);
                    if(f.getName().equals("TABLE_NAME"))
                        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + f.get(c).toString());
                } catch (Exception e) {
                    Log.d("SQLite - ERROR", "Erro ao deletar tabela da classe " + c.getName() + " . Será que falta alguma tabela dependente?");
                }
            }
        }
    }

    public void resetDatabase() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        try {
            dropTables(sqLiteDatabase);
            createTables(sqLiteDatabase);
        } catch (Exception e) {

        }
    }

    public Cursor selectQuery(String tabela, String coluna, long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.query(tabela, null, coluna + "=?", new String[] {""+id}, null, null, null, null);
        return res;
    }

    public Cursor selectALL(String tabela, String coluna) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.query(tabela, null, null, null, null, null, coluna + " ASC");
        return res;
    }

    public long insertQuery(String tabela, ContentValues cv) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.insert(tabela, null, cv);
    }

    public int updateQuery(String tabela, ContentValues cv, long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.update(tabela, cv, "_id=?", new String[] {""+id});
    }

    public int deleteQuery(String tabela, long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(tabela, "_id=?", new String[] {""+id});
    }

    public String getPathFromDB() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.getPath();
    }

    public void exportLocalDatabase() {
        try {
            File sd = Environment.getExternalStorageDirectory();

            if (sd.canWrite()) {
                String currentDBPath = getPathFromDB();
                String backupDBPath = DATABASE_NAME;
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
            Log.e("exportLocalDatabase", "Erro ao exportar o banco pro SD");
        }
    }
}
