package com.example.rodrigo.storewars.AsyncTask;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.rodrigo.storewars.Auxiliar.AdapterProducts;
import com.example.rodrigo.storewars.MainActivity;
import com.example.rodrigo.storewars.Objects.Product;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Rodrigo on 12/12/2017.
 */

public class GetProductsRequestTask extends AsyncTask<Void, String, String> {
    private Exception mLastError = null;
    private MainActivity mainActivity = null;
    ProgressDialog mProgress;

    private String urlRequest = "https://private-841202-stonechallenge.apiary-mock.com/products";

    public GetProductsRequestTask(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    protected void onPreExecute() {
        mProgress = new ProgressDialog(mainActivity);
        mProgress.setTitle("Importando produtos...");
        mProgress.setCancelable(true);
        mProgress.setCanceledOnTouchOutside(false);
        mProgress.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                cancel(false);
            }
        });
        mProgress.show();
    }

    @Override
    protected String doInBackground(Void... voids) {
        try {
            StringBuilder result = new StringBuilder();
            URL url = new URL(urlRequest);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();

            JSONArray jsonArray = new JSONArray(result.toString());
            for(int i = 0; i < jsonArray.length(); i++) {
                if(i != 3){ //não sei porque, apenas essa não consigo pegar a imagem
                    JSONObject jsonobject = jsonArray.getJSONObject(i);
                    String title    = jsonobject.getString("title");
                    double price  = jsonobject.getDouble("price")/100;
                    String zipcode = jsonobject.getString("zipcode");
                    String seller = jsonobject.getString("seller");
                    String thumbnailHd = jsonobject.getString("thumbnailHd");
                    String date = jsonobject.getString("date");
                    URL newurl = new URL(thumbnailHd);
                    Bitmap bitmap = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
                    Product product = new Product(title, price, zipcode, seller, thumbnailHd, bitmap, date);
                    product.insertProduct(mainActivity);
                }
            }
            return result.toString();
        } catch (Exception e) {
            mLastError = e;
            cancel(true);
            return null;
        }
    }

    @Override
    protected void onPostExecute(String output) {
        mProgress.dismiss();
        if (output == null || output.isEmpty()) {
            Toast.makeText(mainActivity, "Erro ao pegar os dados", Toast.LENGTH_SHORT).show();
        } else {
            Product product = new Product();
            AdapterProducts adapterProducts = new AdapterProducts(product.selectProducts(mainActivity), mainActivity);
            mainActivity.listView.setAdapter(adapterProducts);
        }
    }

    @Override
    protected void onCancelled() {
        mProgress.hide();
        if (mLastError != null) {
            Toast.makeText(mainActivity, mLastError.getMessage(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mainActivity, "Pedido cancelado", Toast.LENGTH_SHORT).show();
        }
    }
}
