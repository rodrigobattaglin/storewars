package com.example.rodrigo.storewars;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.example.rodrigo.storewars.Auxiliar.AdapterTransactions;
import com.example.rodrigo.storewars.Objects.Transaction;

public class TransactionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

        ListView listView = (ListView) findViewById(R.id.listTransaction);
        Transaction transaction = new Transaction();
        AdapterTransactions adapterTransactions = new AdapterTransactions(transaction.selectTransactions(this), this);
        listView.setAdapter(adapterTransactions);
    }
}
