package com.example.rodrigo.storewars;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rodrigo.storewars.AsyncTask.CallAPI;
import com.example.rodrigo.storewars.AsyncTask.GetProductsRequestTask;
import com.example.rodrigo.storewars.Auxiliar.AdapterProducts;
import com.example.rodrigo.storewars.Auxiliar.Permission;
import com.example.rodrigo.storewars.Database.SQLite;
import com.example.rodrigo.storewars.Objects.Product;
import com.example.rodrigo.storewars.Objects.Transaction;

import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int MY_PERMISSIONS = 0;
    private String[] PERMISSIONS = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE
    };

    private SQLite sqLite;
    private Permission permission;

    public TextView txtActualBuy;
    public Button btnConfirm;
    public Button btnReset;
    public ListView listView;

    public double price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sqLite = new SQLite(this);

        setContentView(R.layout.activity_main);
    }

    @Override
    public void onStart(){
        super.onStart();
        permission = new Permission(PERMISSIONS);
        loadView();
        if(!permission.hasPermissions(this)){
            ActivityCompat.requestPermissions(this, permission.getPERMISSIONS(), MY_PERMISSIONS);
            sqLite.getWritableDatabase();
            sqLite.exportLocalDatabase();
        } else{
            showProducts();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        if(Permission.hasPermissions(this)) {
            acceptedPermission();
        }
    }

    public void loadView(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        price = 0;

        txtActualBuy = (TextView) findViewById(R.id.txtActualBuy);
        txtActualBuy.setText("Total da Compra Atual: R$" + String.format("%.2f", price));

        btnConfirm = (Button) findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(price != 0) showConfirmDialog();
                else Toast.makeText(MainActivity.this, "Carrinho zerado, adicione algo antes de confirmar", Toast.LENGTH_SHORT).show();
            }
        });

        btnReset = (Button) findViewById(R.id.btnReset);
        btnReset.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                price = 0;
                txtActualBuy.setText("Total da Compra Atual: R$" + String.format("%.2f", price));
            }
        });

        listView = (ListView) findViewById(R.id.listItem);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showAddDialog(i);
            }
        });
    }

    public void acceptedPermission(){
        new GetProductsRequestTask(MainActivity.this).execute();
        showProducts();
    }

    public void showProducts(){
        Product product = new Product();
        AdapterProducts adapterProducts = new AdapterProducts(product.selectProducts(this), this);
        listView.setAdapter(adapterProducts);
    }

    public void showAddDialog(long id){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmação de adição no carrinho");
        builder.setMessage("Quantos itens no carrinho?");

        View dialogView = getLayoutInflater().inflate(R.layout.layout_add_item, null);
        TextView txtInfo = (TextView) dialogView.findViewById(R.id.txtInfo);
        final NumberPicker numberPicker = (NumberPicker) dialogView.findViewById(R.id.np);

        final Product product = new Product(id + 1);
        product.selectProduct(this);

        txtInfo.setText(product.getTitle() + "\n" + "Preço unitário: R$" + String.format("%.2f", product.getPrice()));
        numberPicker.setMaxValue(10);
        numberPicker.setMinValue(1);

        builder.setView(dialogView);
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1) {
                price = price + numberPicker.getValue()*product.getPrice();
                txtActualBuy.setText("Total da Compra Atual: R$" + String.format("%.2f", price));
            }
        });
        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        AlertDialog alerta = builder.create();
        alerta.show();
    }

    public void showConfirmDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmação de compra");
        builder.setMessage("Preencha os campos");

        View dialogView = getLayoutInflater().inflate(R.layout.layout_confirm, null);
        final EditText etCard = (EditText) dialogView.findViewById(R.id.etCartao);
        final EditText etName = (EditText) dialogView.findViewById(R.id.etNome);
        final EditText etValor = (EditText) dialogView.findViewById(R.id.etValor);
        final EditText etCVV = (EditText) dialogView.findViewById(R.id.etCVV);
        final EditText etDate = (EditText) dialogView.findViewById(R.id.etVencimento);

        etValor.setText("R$" + String.format("%.2f", price));

        builder.setView(dialogView);
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1) {
                Transaction transaction = new Transaction(price,
                                                          new Date().toString(),
                                                          Integer.parseInt(etCard.getText().toString().substring(12)),
                                                          etName.getText().toString());
                new CallAPI(MainActivity.this, transaction.insertTransaction(getBaseContext()))
                        .execute(etCard.getText().toString(),
                                 etValor.getText().toString(),
                                 etCVV.getText().toString(),
                                 etName.getText().toString(),
                                 etDate.getText().toString());
            }
        });
        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        AlertDialog alerta = builder.create();
        alerta.show();
    }

    public void viewTransactions() {
        Intent intent = new Intent(this, TransactionActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_main) {

        } else if (id == R.id.nav_transaction) {
            viewTransactions();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
