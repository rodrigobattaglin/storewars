package com.example.rodrigo.storewars.Objects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.rodrigo.storewars.Database.Database;
import com.example.rodrigo.storewars.Database.SQLite;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rodrigo on 12/12/2017.
 */

public class Product {
    private long id;
    private String title;
    private double price;
    private String zipcode;
    private String seller;
    private String urlThumbnail;
    private Bitmap thumbnail;
    private String date;

    public Product(){

    }

    public Product(long id){
        this.id = id;
    }

    public Product(String title, double price, String zipcode, String seller, String urlThumbnail, Bitmap thumbnail, String date) {
        this.title = title;
        this.price = price;
        this.zipcode = zipcode;
        this.seller = seller;
        this.urlThumbnail = urlThumbnail;
        this.thumbnail = thumbnail;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getUrlThumbnail() {
        return urlThumbnail;
    }

    public void setUrlThumbnail(String urlThumbnail) {
        this.urlThumbnail = urlThumbnail;
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean selectProduct(Context context) {
        SQLite sqLite = new SQLite(context);
        Cursor resp = sqLite.selectQuery(Database.ProductsDB.TABLE_NAME, Database.ProductsDB.COLUMN_ID, id);
        if (resp.getCount() > 0){
            resp.moveToFirst();
            for (int i = 0; i < resp.getColumnCount(); i++){
                if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_TITLE)) title = resp.getString(i);
                else if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_PRICE)) price = resp.getDouble(i);
                else if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_ZIPCODE)) zipcode = resp.getString(i);
                else if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_SELLER)) seller = resp.getString(i);
                else if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_URL)) urlThumbnail = resp.getString(i);
                else if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_PHOTO)) thumbnail = BitmapFactory.decodeByteArray(resp.getBlob(i), 0, resp.getBlob(i).length);
                else if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_DATE)) date = resp.getString(i);
            }
        }
        sqLite.close();
        return resp.getCount() > 0;
    }

    public List<Product> selectProducts(Context context) {
        List<Product> listProduct = new ArrayList<Product>();
        SQLite sqLite = new SQLite(context);
        Cursor resp = sqLite.selectALL(Database.ProductsDB.TABLE_NAME, Database.ProductsDB.COLUMN_ID);
        if (resp.getCount() > 0){
            resp.moveToFirst();
            while (!resp.isAfterLast()) {
                Product product = new Product();
                for (int i = 0; i < resp.getColumnCount(); i++){
                    if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_ID)) product.setId(resp.getLong(i));
                    else if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_TITLE)) product.setTitle(resp.getString(i));
                    else if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_PRICE)) product.setPrice(resp.getDouble(i));
                    else if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_ZIPCODE)) product.setZipcode(resp.getString(i));
                    else if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_SELLER)) product.setSeller(resp.getString(i));
                    else if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_URL)) product.setUrlThumbnail(resp.getString(i));
                    else if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_PHOTO)) product.setThumbnail(BitmapFactory.decodeByteArray(resp.getBlob(i), 0, resp.getBlob(i).length));
                    else if(resp.getColumnName(i).equals(Database.ProductsDB.COLUMN_DATE)) product.setDate(resp.getString(i));
                }
                listProduct.add(product);
                resp.moveToNext();
            }
        }
        sqLite.close();
        return listProduct;
    }

    public byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    public long insertProduct(Context context){
        SQLite sqLite = new SQLite(context);
        ContentValues cv = new ContentValues();

        cv.put(Database.ProductsDB.COLUMN_TITLE, title);
        cv.put(Database.ProductsDB.COLUMN_PRICE, price);
        cv.put(Database.ProductsDB.COLUMN_ZIPCODE, zipcode);
        cv.put(Database.ProductsDB.COLUMN_SELLER, seller);
        cv.put(Database.ProductsDB.COLUMN_URL, urlThumbnail);
        cv.put(Database.ProductsDB.COLUMN_PHOTO, getBytes(thumbnail));
        cv.put(Database.ProductsDB.COLUMN_DATE, date);
        id = sqLite.insertQuery(Database.ProductsDB.TABLE_NAME, cv);
        sqLite.exportLocalDatabase();
        sqLite.close();
        return id;
    }
}
